#!/usr/bin/env -S pipenv run python
# vim: cc=88 tw=88 ts=4 sts=4 sw=4 et

# SPDX-FileCopyrightText: 2023 Mario-ttide
# SPDX-License-Identifier: AGPL-3.0-or-later

import asyncio
import html
import locale
import logging
import os
import re
import signal
import sys
from collections import defaultdict

import asyncpg
import orjson
import uvloop
from aiolimiter import AsyncLimiter
from telethon import TelegramClient, events
from telethon.errors import FloodWaitError
from telethon.errors.rpcerrorlist import AccessTokenExpiredError

from telethon.network.connection.tcpabridged import ConnectionTcpAbridged
from telethon.tl.functions.bots import SetBotCommandsRequest
from telethon.tl.functions.channels import GetFullChannelRequest
from telethon.tl.functions.messages import GetFullChatRequest
from telethon.tl.functions.users import GetFullUserRequest
from telethon.tl.types import (
    BotCommandScopeDefault,
    Channel,
    ChannelParticipant,
    ChannelParticipantAdmin,
    ChannelParticipantBanned,
    ChannelParticipantCreator,
    ChannelParticipantsAdmins,
    ChannelParticipantSelf,
    ChatInviteExported,
    ChatParticipant,
    ChatParticipantAdmin,
    ChatParticipantCreator,
    InputPeerChannel,
    InputPeerChat,
    InputPeerUser,
    UpdateChannelParticipant,
    UpdateChatParticipant,
    User,
)
from telethon.utils import get_display_name, get_peer_id

PROGRAM_NAME = os.path.basename(os.path.dirname(os.path.abspath(sys.argv[0])))
VALID_USERNAME = re.compile(r"[a-zA-Z][a-zA-Z0-9_]{4,31}")

api_id = int(os.environ.get("TG_API_ID"))
api_hash = os.environ.get("TG_API_HASH")
bot_token = os.environ.get("TG_BOT_TOKEN")
debug_bot_token = os.environ.get("DEBUG_BOT_TOKEN")

# Remove some noise
logging.getLogger("apscheduler").setLevel(logging.WARNING)

logging.basicConfig(level=logging.INFO)


class BotClone(TelegramClient):
    clients = set()
    create_lock = asyncio.Lock()

    @classmethod
    async def before_create(cls, pool):
        """Placeholder"""

    @classmethod
    async def create(cls, pool, username):
        """Create a new initialized class"""

        async with cls.create_lock:
            # Avoid launching the same bot multiple times
            for client in cls.clients:
                if client.username == username:
                    return None

        # Paranoia
        if not VALID_USERNAME.fullmatch(username):
            return None

        self = cls(
            username,
            api_id,
            api_hash,
            connection=ConnectionTcpAbridged,
            base_logger=f"{username}.telethon",
            catch_up=True,
        )

        self.parse_mode = "html"

        # Silent "Got difference for" messages
        logging.getLogger(f"{username}.telethon.client.updates").setLevel(
            level=logging.WARNING
        )

        self.pool = pool
        self.username = username
        self.last_event = {}
        self.logger = logging.getLogger(username)
        self.locks = defaultdict(lambda: asyncio.Lock())
        self.limiters = {}

        bot_token = await self._get_token(username)

        self.add_event_handler(
            self._ping_hndlr, events.NewMessage(incoming=True, pattern=r"^/ping$")
        )
        self.add_event_handler(
            self._staff_hndlr,
            events.NewMessage(
                incoming=True,
                func=lambda e: e.is_private,
                pattern=r"/staff(@\w+)? @?(?P<chat_id>-[0-9]+)$",
            ),
        )
#        self.add_event_handler(
#            self._update_user_hndlr, events.NewMessage(incoming=True)
#        )
        self.add_event_handler(
            self._chat_member_hndlr,
            events.Raw(types=(UpdateChannelParticipant, UpdateChatParticipant)),
        )

        if self._clone_hndlr:
            self.add_event_handler(
                self._clone_hndlr,
                events.NewMessage(
                    incoming=True, func=lambda e: e.is_private, pattern=r"/clone$"
                ),
            )

            self.add_event_handler(
                self._parse_botfather_msg_hndlr,
                events.NewMessage(
                    incoming=True,
                    func=lambda e: e.is_private
                    and e.forward
                    and getattr(e.forward.from_id, "user_id", None) == 93372553,
                ),
            )
            self.add_event_handler(
                self._stats_hndlr,
                events.NewMessage(
                    incoming=True, func=lambda e: e.is_private, pattern=r"/stats$"
                ),
            )

        for i in range(1, 10):
            try:
                await asyncio.shield(self.start(bot_token=bot_token))
            except AccessTokenExpiredError:
                await self._delete_bot(username)
                return None
            except asyncio.CancelledError:
                # The clone may be revoked, so delete the session file and ignore it
                try:
                    os.remove(f"{username}.session")
                except OSError:
                    pass
                return None
            except Exception:
                self.logger.exception("start")
                await asyncio.sleep(i)
            else:
                break

        self.logger.info("Bot initialized")

        self._create_task = self.loop.create_task(self.run_until_disconnected())

        cls.clients.add(self)

        return self

    async def start(self, *args, **kwargs):
        """Launch start from parent and update user_id in table bot"""

        await super().start(*args, **kwargs)
        me = await self.get_me(True)
        async with self.pool.acquire() as conn:
            await conn.execute(
                """UPDATE "telegrambots"."bots" SET "user_id" = $1
                   WHERE "programname" = $2
                         AND ( "user_id" IS NULL OR "user_id" != $1 )""",
                me.user_id,
                self.username,
            )
        return self

    async def _delete_bot(self, username):
        """Delete the bot"""

        self.logger.info("Deleting bot...")
        async with self.pool.acquire() as conn:
            await conn.execute(
                'DELETE FROM "telegrambots"."bots" WHERE "programname" = $1',
                username,
            )
            try:
                await conn.execute(f'DROP SCHEMA IF EXISTS "{self.schema}" CASCADE')
            except AttributeError:
                pass
        await self.disconnect()
        try:
            os.remove(f"{username}.session")
        except OSError:
            pass
        self.clients.discard(self)
        self.logger.warning("Bot deleted successfully")

    async def _get_token(self, username):
        async with self.pool.acquire() as conn:
            bot_token = await conn.fetchval(
                """SELECT "token" FROM "telegrambots"."bots"
                   WHERE "programname" = $1""",
                username,
            )
        return bot_token

    # HANDLERS
    # Common
    async def _chat_member_hndlr(self, update):
        """Updates user_in_chat when an user join/leave"""

        if isinstance(update, UpdateChatParticipant):
            chat_id = -update.chat_id
            was_member = type(update.prev_participant) in [
                ChatParticipantAdmin,
                ChatParticipant,
            ]
            is_member = type(update.new_participant) in [
                ChatParticipantAdmin,
                ChatParticipant,
            ]
        elif isinstance(update, UpdateChannelParticipant):
            chat_id = -(1000000000000 + update.channel_id)

            was_member = type(update.prev_participant) in [
                ChannelParticipantSelf,
                ChannelParticipantAdmin,
                ChannelParticipant,
            ] or (
                isinstance(update.prev_participant, ChannelParticipantBanned)
                and not update.prev_participant.left
            )

            is_member = isinstance(
                update.new_participant,
                (
                    ChannelParticipantSelf,
                    ChannelParticipantAdmin,
                    ChannelParticipant,
                ),
            ) or (
                isinstance(update.new_participant, ChannelParticipantBanned)
                and not update.new_participant.left
            )

        if was_member == is_member:
            return

        me = await self.get_me(True)

        if not was_member and update.user_id == me.user_id:
            members = 0
            chat_invite_link = None
            chat_description = None
            try:
                chat = await self.get_entity(chat_id)
                ch_full = await self.get_full_entity(chat_id)
                if isinstance(chat, Channel):
                    members = ch_full.full_chat.participants_count
                    if chat.megagroup:
                        chat_type = "supergroup"
                    else:
                        chat_type = "channel"
                else:
                    chat_type = "group"
                if isinstance(ch_full.full_chat.exported_invite, ChatInviteExported):
                    chat_invite_link = ch_full.full_chat.exported_invite.link
                chat_description = ch_full.full_chat.about
            except Exception:
                self.logger.exception(f"_chat_member_hndlr {chat_id}")
            else:
                async with self.pool.acquire() as conn:
                    await conn.execute(
                        """INSERT INTO "chats"("id", "title", "type", "username",
                                               "description", "invite_link", "members")
                           VALUES ($1, $2, $3, $4, $5, $6, $7)
                           ON CONFLICT ("id") DO UPDATE SET
                           "title" = EXCLUDED."title",
                           "username" = EXCLUDED."username",
                           "description" = EXCLUDED."description",
                           "invite_link" = EXCLUDED."invite_link",
                           "members" = EXCLUDED."members",
                           "last_update" = CURRENT_TIMESTAMP""",
                        chat_id,
                        chat.title,
                        chat_type,
                        getattr(chat, "username", None),
                        chat_description,
                        chat_invite_link,
                        members or 0,
                    )

        # user_in_chat doesn't have any dependency, so it should be safe to just insert
        # on it directly
        async with self.pool.acquire() as conn:
            await conn.execute(
                '''INSERT INTO "user_in_chat"("user_id", "chat_id", "left")
                   VALUES ($1, $2, $3) ON CONFLICT ("user_id", "chat_id") DO
                   UPDATE SET "last_update" = CURRENT_TIMESTAMP,
                              "left" = EXCLUDED."left"''',
                update.user_id,
                chat_id,
                was_member,
            )

    async def _update_user_hndlr(self, event):
        """Updates chats, users and user_in_chat on each message"""

        chat = await event.get_chat()
        user = await event.get_sender()

        me = await self.get_me(True)

        # user_in_chat doesn't have any dependency, so it should be safe to just insert
        # on it directly
        if user and chat and not isinstance(chat, User):
            async with self.pool.acquire() as conn:
                for user_id in (user.id, me.user_id):
                    await conn.execute(
                        """INSERT INTO "user_in_chat"("user_id", "chat_id")
                           VALUES ($1, $2) ON CONFLICT ("user_id", "chat_id") DO
                           UPDATE SET "last_update" = CURRENT_TIMESTAMP""",
                        user_id,
                        get_peer_id(chat),
                    )

        if user and isinstance(user, User):
            async with self.pool.acquire() as conn:
                async with conn.transaction():
                    await conn.execute(
                        """UPDATE "users" SET "username" = NULL
                           WHERE "id" <> $1 AND "username" = $2""",
                        user.id,
                        user.username,
                    )

                    await conn.execute(
                        """INSERT INTO "users"
                           ("id", "is_bot", "first_name", "last_name", "username")
                           VALUES ($1, $2, $3, $4, $5)
                           ON CONFLICT (id) DO UPDATE SET
                           "is_bot" = EXCLUDED."is_bot",
                           "first_name" = EXCLUDED."first_name",
                           "last_name" = EXCLUDED."last_name",
                           "username" = EXCLUDED."username",
                           "last_update" = CURRENT_TIMESTAMP""",
                        user.id,
                        user.bot,
                        user.first_name,
                        user.last_name,
                        user.username,
                    )

                row = await conn.fetchrow(
                    """SELECT "id", "first_name", "last_name", "username"
                       FROM "users_changes" WHERE "id" = $1
                       ORDER BY "update" DESC LIMIT 1""",
                    user.id,
                )
                if row is None:
                    await conn.execute(
                        """INSERT INTO "users_changes"
                           ("id", "first_name", "last_name", "username")
                           VALUES ($1, $2, $3, $4)""",
                        user.id,
                        user.first_name,
                        user.last_name,
                        user.username,
                    )
                else:
                    if (
                        row["first_name"] != user.first_name
                        or row["last_name"] != user.last_name
                        or row["username"] != user.username
                    ):
                        await conn.execute(
                            """INSERT INTO "users_changes"
                               ("id", "first_name", "last_name", "username",
                                "changed_first_name", "changed_last_name",
                                "changed_username")
                               VALUES ($1, $2, $3, $4, $5, $6, $7)""",
                            user.id,
                            user.first_name,
                            user.last_name,
                            user.username,
                            row["first_name"] != user.first_name,
                            row["last_name"] != user.last_name,
                            row["username"] != user.username,
                        )

        if chat and not isinstance(chat, User):
            async with self.pool.acquire() as conn:
                last_update = await conn.fetchval(
                    """SELECT "last_update" < NOW() - INTERVAL '1 HOUR'
                       FROM "chats" WHERE "id" = $1""",
                    get_peer_id(chat),
                )
            members = 0
            chat_invite_link = None
            chat_description = None
            if last_update is not False:
                ch_full = await self.get_full_entity(chat)
                if isinstance(chat, Channel):
                    members = ch_full.full_chat.participants_count
                if isinstance(ch_full.full_chat.exported_invite, ChatInviteExported):
                    chat_invite_link = ch_full.full_chat.exported_invite.link
                chat_description = ch_full.full_chat.about

            if isinstance(chat, Channel):
                if chat.megagroup:
                    chat_type = "supergroup"
                else:
                    chat_type = "channel"
            else:
                chat_type = "group"
            async with self.pool.acquire() as conn:
                await conn.execute(
                    """INSERT INTO "chats"("id", "title", "type",
                                           "username", "description",
                                           "invite_link", "members")
                       VALUES ($1, $2, $3, $4, $5, $6, $7)
                       ON CONFLICT ("id") DO UPDATE SET
                       "title" = EXCLUDED."title",
                       "username" = EXCLUDED."username",
                       "description" = COALESCE(EXCLUDED."description", "chats"."description"),
                       "invite_link" = COALESCE(EXCLUDED."invite_link", "chats"."invite_link"),
                       "members" = COALESCE(NULLIF(EXCLUDED."members", 0), "chats"."members"),
                       "last_update" = CURRENT_TIMESTAMP""",
                    get_peer_id(chat),
                    chat.title,
                    chat_type,
                    getattr(chat, "username", None),
                    chat_description,
                    chat_invite_link,
                    members,
                )

    async def _ping_hndlr(self, event):
        """Reply to /ping with PONG"""

        await event.reply("PONG")

    async def _staff_hndlr(self, event):
        """Get the list of staff members in a chat"""

        chat_id = int(event.pattern_match.group("chat_id"))
        chat = await self.get_entity(chat_id)
        escaped_title = html.escape(chat.title)
        txt = f"Lista admin in {escaped_title} [<code>{chat.id}</code>]:\n"
        members = {"creator": [], "cofounders": [], "admins": []}
        async for user in self.iter_participants(
            chat_id, filter=ChannelParticipantsAdmins
        ):
            permissions = await self.get_permissions(chat, user)
            if permissions.is_creator:
                members["creator"] = [(user, permissions)]
            elif permissions.add_admins:
                members["cofounders"].append((user, permissions))
            else:
                members["admins"].append((user, permissions))
        for role in ("creator", "cofounders", "admins"):
            if len(members[role]):
                if role == "creator":
                    txt += "\n👑 Fondatore\n"
                elif role == "cofounders":
                    txt += "\n⚜️ Vice-Fondatore\n"
                elif role == "admins":
                    txt += "\n👮🏼 Amministratore\n"
                for user, member in members[role]:
                    escaped_name = html.escape(
                        get_display_name(user) or "Deleted Account"
                    )
                    link = f'<a href="tg://user?id={user.id}">{escaped_name}</a>'
                    if member.anonymous:
                        link = f"<s>{link}</s>"
                    if user.bot:
                        link += " 🤖"
                    if user.username:
                        link += f" (@{user.username})"
                    link += f" [<code>{user.id}</code>]"
                    txt += f"└ {link}\n"

        await event.reply(txt, parse_mode="HTML")

    async def _clone_hndlr(self, event):
        """Send instructions after /clone"""

        user = await event.get_sender()
        if user.lang_code == "it":
            await event.reply(
                "<b>Clona questo bot</b>\n"
                "I cloni sono copie identiche di questo bot.\n\n"
                "<b>Per creare un clone:</b>\n"
                " • Vai su @BotFather\n"
                " • Lancia /newbot\n"
                " • Digita il nome che vuoi dare al bot\n"
                " • Digita lo username che vuoi dare al bot\n"
                " • Inoltra qui (su questo bot) il messaggio che ricevi da @BotFather\n"
                " • Fatto",
                parse_mode="HTML",
            )
        else:
            await event.reply(
                "<b>Clone this bot</b>\n"
                "Clones are identically copies of this bot.\n\n"
                "<b>To create a clone:</b>\n"
                " • Go to @BotFather\n"
                " • Launch /newbot\n"
                " • Write the name you want to give to the bot\n"
                " • Write the username you want to give to the bot\n"
                " • Forward the message you receive from@BotFather here\n"
                " • Done",
                parse_mode="HTML",
            )

    async def _parse_botfather_msg_hndlr(self, event):
        """Parse token sent from @BotFather"""

        user = await event.get_sender()

        if event.raw_text.startswith(
            "Done! Congratulations on your new bot. You will find it at "
        ):
            username = event.raw_text[64:].split(".")[0]
            token = event.raw_text.split("\n")[3]
        elif event.raw_text.startswith("Token for the bot "):
            username = event.raw_text.split("\n")[0].split(" ")[-7][1:]
            token = event.raw_text.split("\n")[2]
        elif event.raw_text.startswith("Here is the token for bot "):
            username = event.raw_text.split("\n")[0].split(" ")[-1][1:-1]
            token = event.raw_text.split("\n")[2]
        else:
            await event.reply(
                "Devi inoltrare il messaggio da @BotFather che "
                "inizia per:\n"
                "`Done! Congratulations on your new bot.`\n"
                "o:\n"
                "`Token for the bot ... has been revoked.`\n"
                "o:\n"
                "`Here is the token for bot`",
                parse_mode="Markdown",
            )
            return

        async with self.pool.acquire() as conn:
            await conn.execute(
                '''INSERT INTO "telegrambots"."bots"
                   (programname, token, port, cloned_by_user_id, clone_of)
                   VALUES ($1, $2, NULL, $3, $4) ON CONFLICT("programname") DO UPDATE SET
                   "token" = EXCLUDED."token",
                   "port" = EXCLUDED."port",
                   "cloned_by_user_id" = EXCLUDED."cloned_by_user_id",
                   "clone_of" = EXCLUDED."clone_of"''',
                username,
                token,
                user.id,
                PROGRAM_NAME,
            )

        try:
            try:
                os.remove(f"{username}.session")
            except OSError:
                pass
            await type(self).create(self.pool, username)
        except Exception:
            self.logger.exception(f"_parse_botfather_msg_hndlr {username}")
            if user.lang_code == "it":
                await event.reply(
                    "Bot <b>NON</b> clonato con successo...", parse_mode="HTML"
                )
            else:
                await event.reply(
                    "Bot <b>NOT</b> cloned successfully...", parse_mode="HTML"
                )
        else:
            if user.lang_code == "it":
                await event.reply("Bot clonato con successo...")
            else:
                await event.reply("Bot cloned successfully...")

    async def _stats_hndlr(self, event):
        """Send statistics with /stats command"""

        user = await event.get_sender()

        wait_message = await event.reply("Please wait...")

        async with self.pool.acquire() as conn:
            if bot_token:
                clones = await conn.fetchval(
                    'SELECT COUNT(*) FROM "telegrambots"."bots" WHERE "token" = $1',
                    bot_token,
                )
            else:
                clones = await conn.fetchval(
                    """SELECT COUNT(*) FROM "telegrambots"."bots"
                       WHERE "programname" = $1 OR "clone_of" = $1""",
                    PROGRAM_NAME,
                )
            rows = await conn.fetch(
                '''SELECT COUNT(*), "left" FROM "telegrambots"."user_in_chat"
                   JOIN "telegrambots"."bots" ON "bots"."user_id" = "user_in_chat"."user_id"
                   WHERE "clone_of" = $1 GROUP BY "left"''',
                PROGRAM_NAME,
            )

        chats = {row["left"]: row["count"] for row in rows}

        await wait_message.delete()
        if user.lang_code == "it":
            await event.reply(
                f"Il bot è stato clonato {clones} volte ed è presente su"
                f" {chats.get(False, 0)} chat.\nÈ stato rimosso da"
                f" {chats.get(True, 0)} chat.\nNumero di connessioni attuali verso il"
                f" database: {self.pool.get_size()}\nNumero di connessioni inattive"
                f" verso il database: {self.pool.get_idle_size()}"
            )
        else:
            await event.reply(
                f"This bot has {clones} clones and it's on {chats.get(False, 0)} chat(s)"
                f".\nIt was removed from {chats.get(True, 0)} chat(s).\n"
                f"Current number of db connections: {self.pool.get_size()}\n"
                f"Current number of idle db connections: {self.pool.get_idle_size()}"
            )

    # UTILITIES

    async def set_bot_commands(
        self, commands, scope=BotCommandScopeDefault(), lang_code=""
    ):
        """Set the list of the bot's commands"""

        return await self(
            SetBotCommandsRequest(scope=scope, lang_code=lang_code, commands=commands)
        )

    async def get_full_entity(self, entity):
        """Get the "full" entity, but with correct limited to avoid FloodWait"""
        try:
            peer = await self.get_input_entity(entity)
        except ValueError:
            return None

        entity_id = get_peer_id(entity)
        kind = type(peer).__name__
        async with self.locks[kind]:
            try:
                limiter = self.limiters[kind]
            except KeyError:
                limiter = self.limiters[kind] = AsyncLimiter(3, 30)
        async with limiter:
            try:
                match peer:
                    case InputPeerChannel():
                        request = GetFullChannelRequest(peer)
                    case InputPeerChat():
                        request = GetFullChatRequest(peer.chat_id)
                    case InputPeerUser():
                        request = GetFullUserRequest(peer)

                return await self(request, flood_sleep_threshold=0)
            except FloodWaitError as e:
                self.logger.warning(
                    "Waiting for %d seconds before continuing (required by"
                    ' "get_full_entity(%d)")',
                    e.seconds,
                    entity_id,
                )
                await asyncio.sleep(e.value)
                return await self.get_full_entity(entity)
            except Exception:
                #                self.logger.exception("%s(%d)", type(request).__name__, entity_id)
                raise
            return None

    async def get_admin_ids(self, chat_id):
        """Get admin ids of a chat"""

        ret = {None}

        async with self.pool.acquire() as conn:
            admins = await conn.fetch(
                """SELECT "user_id" FROM "admin_in_chat"
                   JOIN "chats" ON "id" = "chat_id"
                   WHERE "chat_id" = $1
                   AND ( "creator" OR "type" NOT IN ( 'supergroup', 'channel' )
                         OR "privileges" IS NOT NULL )
                   AND "admin_in_chat"."last_update" > NOW() - INTERVAL '10 MINUTE'""",
                chat_id,
            )
        if admins:
            return {x[0] for x in admins}

        chat = await self.get_entity(chat_id)

        async for user in self.iter_participants(
            chat, filter=ChannelParticipantsAdmins
        ):
            if user.bot:
                continue
            j = []
            creator = isinstance(
                user.participant, ChannelParticipantCreator
            ) or isinstance(user.participant, ChatParticipantCreator)
            if creator:
                ret.add(user.id)
            elif not isinstance(chat, Channel):
                ret.add(user.id)
            else:
                admin_rights = user.participant.admin_rights
                if admin_rights.anonymous:
                    j.append("is_anonymous")
                if admin_rights.add_admins:
                    j.append("can_promote_members")
                    ret.add(user.id)
                if admin_rights.ban_users:
                    j.append("can_restrict_members")
                    ret.add(user.id)
                if admin_rights.change_info:
                    j.append("can_change_info")
                    ret.add(user.id)
                if admin_rights.delete_messages:
                    j.append("can_delete_messages")
                    ret.add(user.id)
                if admin_rights.delete_stories:
                    j.append("can_delete_stories")
                    ret.add(user.id)
                if admin_rights.edit_messages:
                    j.append("can_edit_messages")
                    ret.add(user.id)
                if admin_rights.edit_stories:
                    j.append("can_edit_stories")
                    ret.add(user.id)
                if admin_rights.invite_users:
                    j.append("can_invite_users")
                    ret.add(user.id)
                if admin_rights.manage_call:
                    j.append("can_manage_video_chats")
                    ret.add(user.id)
                if admin_rights.manage_topics:
                    j.append("can_manage_topics")
                    ret.add(user.id)
                if admin_rights.pin_messages:
                    j.append("can_pin_messages")
                    ret.add(user.id)
                if admin_rights.post_messages:
                    j.append("can_post_messages")
                    ret.add(user.id)
                if admin_rights.post_stories:
                    j.append("can_post_stories")
                    ret.add(user.id)

            async with self.pool.acquire() as conn:
                await conn.execute(
                    """INSERT INTO "admin_in_chat"
                       ("user_id", "chat_id", "creator", "privileges")
                       VALUES ($1, $2, $3, $4) ON CONFLICT ("user_id", "chat_id") DO
                       UPDATE SET "creator" = EXCLUDED."creator",
                                  "privileges" = EXCLUDED."privileges",
                                  "last_update" = CURRENT_TIMESTAMP""",
                    user.id,
                    chat_id,
                    creator,
                    j and orjson.dumps(j).decode() or None,
                )

        return ret

    async def get_user_id(self, username):
        """Get user_id from username (or None if not found)"""

        # ensure valid userid
        if len(username) <= 5:
            return None

        if username.isdigit():
            return int(username)

        if username.startswith("@"):
            username = username[1:]

        async with self.pool.acquire() as conn:
            return await conn.fetchval(
                'SELECT "id" FROM "users" WHERE LOWER("username") = $1',
                username.casefold(),
            )

    @classmethod
    async def clone_task(cls, pool):
        """Clone the bot if needed"""
        while True:
            try:
                async with pool.acquire() as conn:
                    rows = await conn.fetch(
                        '''SELECT "programname" FROM "telegrambots"."bots"
                           WHERE "clone_of" = $1 AND "to_be_cloned"''',
                        PROGRAM_NAME,
                    )
                for row in rows:
                    username = row[0]
                    try:
                        ret = await cls.create(pool, username)
                    except Exception:
                        logging.exception("clone_task")
                    else:
                        logging.info(f"{username} [{not not ret}]")
                        async with pool.acquire() as conn:
                            await conn.execute(
                                """UPDATE "telegrambots"."bots"
                                   SET "to_be_cloned" = FALSE
                                   WHERE "programname" = $1""",
                                username,
                            )
            finally:
                await asyncio.sleep(60)

    @staticmethod
    async def get_clones(pool):
        """Get the list of clones"""
        async with pool.acquire() as conn:
            if bot_token:
                rows = await conn.fetch(
                    """SELECT "programname" FROM "telegrambots"."bots"
                       WHERE "token" = $1 ORDER BY "user_id" ASC""",
                    bot_token,
                )
            else:
                rows = await conn.fetch(
                    """SELECT "programname" FROM "telegrambots"."bots"
                       WHERE "programname" = $1 OR "clone_of" = $1
                       ORDER BY "user_id" ASC""",
                    PROGRAM_NAME,
                )
        return tuple(row[0] for row in rows)


async def _main(cls):
    """async main that creates the pool, get the clones and initialize the classes"""

    loop = asyncio.get_running_loop()
    loop.add_signal_handler(signal.SIGTERM, loop.stop)

    pool = await asyncpg.create_pool(
        os.environ.get("POSTGRES_DSN"),
        server_settings={"application_name": PROGRAM_NAME},
        min_size=5,
        max_size=50,
        max_inactive_connection_lifetime=60.0,
    )

    await cls.before_create(pool)

    clones = await cls.get_clones(pool)

    clone_task = loop.create_task(cls.clone_task(pool))

    for username in clones:
        await cls.create(pool, username)

    logging.info("Initialization completed")

    try:
        # Wait for all tasks to be completed
        while not all(client._create_task.done() is True for client in cls.clients):
            await asyncio.sleep(1)
    finally:
        clone_task.cancel()
        for client in cls.clients.copy():
            await client.disconnect()
            client.logger.info("Bot shutdown")
        logging.info("Shutdown completed")


def main(cls):
    """Just a main that calls _main with asyncio"""

    locale.setlocale(locale.LC_TIME, "it_IT.UTF-8")
    try:
        uvloop.run(_main(cls))
    except (KeyboardInterrupt, RuntimeError, SystemExit):
        pass


if __name__ == "__main__":
    main(BotClone)
